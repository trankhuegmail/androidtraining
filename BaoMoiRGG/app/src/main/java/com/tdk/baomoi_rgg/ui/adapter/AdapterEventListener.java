package com.tdk.baomoi_rgg.ui.adapter;

import com.tdk.baomoi_rgg.demo.Article;

public interface AdapterEventListener {
    interface SetOnItemClickListener {
        void onItemClick(Article article);
    }
}
