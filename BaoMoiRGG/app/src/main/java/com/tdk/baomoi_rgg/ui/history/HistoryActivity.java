package com.tdk.baomoi_rgg.ui.history;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.db.model.Article;
import com.tdk.baomoi_rgg.ui.article.ArticleActivity;
import com.tdk.baomoi_rgg.ui.edit_history.EditHistoryActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class HistoryActivity extends AppCompatActivity implements HistoryMvpView {
    @BindView(R.id.recycler_view_history) RecyclerView mRecyclerView;
    private Unbinder unbinder;
    private HistoryAdapter mAdapter;
    private ArrayList<Article> mArticleArrayList;
    private Realm mRealm;
    private HistoryPresenter mPresenter;
    private PopupMenu mPopupMenu;
    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        getSupportActionBar().setTitle(R.string.activity_history_title_bar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        unbinder   = ButterKnife.bind(this);
        try {
            mActionBar = getSupportActionBar();
            mActionBar.setTitle(R.string.activity_history_title_bar);
            mActionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {

        }

        initPresenter();
        initRealm();
        setAdapter();

        Intent getData = getIntent();
        loadData(getData);
    }

    private void initPresenter() {
        mPresenter = new HistoryPresenter(this);
    }

    private void initRealm() {
        Realm.init(this);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("history.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();

        mRealm = Realm.getInstance(configuration);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!mRealm.isClosed()) {
            mRealm.close();
        }
        unbinder.unbind();
    }

    @Override
    public void setAdapter() {
//        RealmResults<Article> articles = mRealm.where(Article.class).findAll();
        RealmResults<Article> articles = mPresenter.getDataRealm(mRealm);

        mArticleArrayList = new ArrayList<>();
        for (int i=0; i<articles.size(); i++) {
            mArticleArrayList.add(i, articles.get(i));
        }

        if (mArticleArrayList != null) {
            mAdapter = new HistoryAdapter(mArticleArrayList, HistoryActivity.this);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

            adapterEventsListener();
        }
    }

    private void adapterEventsListener() {
        mAdapter.setOnItemClickListener(new HistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                setPopupMenu(view.getContext(), view);
                popupMenuClickListener(position);
            }
        });
    }

    private void popupMenuClickListener(int index) {
        mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit:
                        Bundle bundle = new Bundle();
                        bundle.putInt("id", mArticleArrayList.get(index).getId());
                        bundle.putString("title", mArticleArrayList.get(index).getTitle());
                        bundle.putString("author", mArticleArrayList.get(index).getAuthor());
                        bundle.putString("content", mArticleArrayList.get(index).getContent());
                        bundle.putString("published_at", mArticleArrayList.get(index).getPublishedAt());
                        bundle.putString("url_to_image", mArticleArrayList.get(index).getUrlToImage());

                        Intent intent = new Intent(HistoryActivity.this, EditHistoryActivity.class);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                        return true;
                    case R.id.delete:
                        mPresenter.deleteDataRealm(mRealm, mArticleArrayList, index, "id");
                        mPresenter.removeItemAdapter(mAdapter, mArticleArrayList, index);

//                        mRealm.executeTransactionAsync(new Realm.Transaction() {
//                            @Override
//                            public void execute(Realm realm) {
//                                Article result = realm.where(Article.class)
//                                        .equalTo("id", mArticleArrayList.get(index).getId())
//                                        .findFirst();
//                                result.deleteFromRealm();
//                            }
//                        },new Realm.Transaction.OnSuccess() {
//                            @Override
//                            public void onSuccess() {
//                                mArticleArrayList.remove(index);
////                                mRecyclerView.removeViewAt(index);
//                                mAdapter.notifyItemRemoved(index);
//                                mAdapter.notifyItemRangeChanged(index, mArticleArrayList.size());
//                                mAdapter.notifyDataSetChanged();
//                            }
//                        });
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public void setPopupMenu(Context context, View view) {
        mPopupMenu = new PopupMenu(context, view);
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_article_odd, mPopupMenu.getMenu());
        mPopupMenu.show();
    }

    @Override
    public void loadData(Intent getData) {
        if(mPresenter.getBundle(getData, "edit") != null) {
            mRealm.beginTransaction();

            Article article = mRealm.where(Article.class)
                    .equalTo("id"
                        , mPresenter.getBundle(getData, "edit").getInt("id"))
                    .findFirst();
            article.setTitle(mPresenter.getBundle(getData, "edit").getString("title"));

            mRealm.commitTransaction();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), ArticleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, 0);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ArticleActivity.exit) {
            finish();
            return;
        }
    }
}
