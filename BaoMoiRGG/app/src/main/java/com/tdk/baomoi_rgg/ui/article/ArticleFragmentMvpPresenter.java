package com.tdk.baomoi_rgg.ui.article;

import io.reactivex.disposables.CompositeDisposable;

public interface ArticleFragmentMvpPresenter {
    void start();
    void getArticles(CompositeDisposable compositeDisposable, String country, String apiKey, int pageIndex, int pageSize);
}
