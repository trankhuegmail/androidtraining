package com.tdk.baomoi_rgg.ui.history;

import android.content.Context;
import android.content.Intent;
import android.view.View;

public interface HistoryMvpView {
    void setAdapter();
    void setPopupMenu(Context context, View view);
    void loadData(Intent getData);
}
