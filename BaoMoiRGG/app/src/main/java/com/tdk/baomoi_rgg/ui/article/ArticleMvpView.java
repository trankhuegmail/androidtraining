package com.tdk.baomoi_rgg.ui.article;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.tabs.TabLayout;
import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

public interface ArticleMvpView {
    void init();
    void showTabLayout(TabLayout mTabLayout, int mIndex, FragmentManager mFragmentManager, Fragment mFragment);
}
