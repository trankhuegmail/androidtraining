package com.tdk.baomoi_rgg.ui.article;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;
import com.tdk.baomoi_rgg.R;

public class ArticlePresenter implements ArticleMvpPresenter {
    private ArticleMvpView mView;

    public ArticlePresenter(ArticleMvpView mView) {
        this.mView = mView;
    }

    @Override
    public void start() {
        mView.init();
    }

    @Override
    public void setDefaultTabLayout(TabLayout mTabLayout, int mIndex, FragmentManager mFragmentManager, Fragment mFragment) {
        mView.showTabLayout(mTabLayout, mIndex, mFragmentManager, mFragment);
    }

    @Override
    public void loadArticle(FragmentManager mFragmentManager, Fragment mFragment, int id) {
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(id, mFragment).commit();
    }
}
