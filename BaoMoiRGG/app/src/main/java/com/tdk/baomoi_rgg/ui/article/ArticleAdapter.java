package com.tdk.baomoi_rgg.ui.article;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tdk.baomoi_rgg.ui.details_article.DetailArticleActivity;
import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ArticleResponse> articles;
    private Context mContext;

    public ArticleAdapter(ArrayList<ArticleResponse> articles, Context mContext) {
        this.articles = articles;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == 1) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article_odd, parent, false);
            vh = new ArticleAdapter.ArticleOddViewHolder(v);
        } else if (viewType == 2) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article_even, parent, false);
            vh = new ArticleAdapter.ArticleEvenViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ArticleAdapter.ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArticleAdapter.ArticleOddViewHolder) {
            ((ArticleAdapter.ArticleOddViewHolder) holder).onBind(articles.get(position));
        } else if (holder instanceof ArticleAdapter.ArticleEvenViewHolder) {
            ((ArticleAdapter.ArticleEvenViewHolder) holder).onBind(articles.get(position));
        } else if (holder instanceof ArticleAdapter.ProgressViewHolder) {
            ((ArticleAdapter.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (articles.get(position) != null) {
            if (position % 2 !=0) {
                return 1;
            }else {
                return 2;
            }
        }else {
            return 0;
        }
    }

    public class ArticleOddViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_view_author) TextView tvAuthor;
        @BindView(R.id.text_view_title) TextView tvTitle;
        @BindView(R.id.text_view_published_at) TextView tvPublishedAt;
        @BindView(R.id.image_url) ImageView imgUrl;
        private View mView;
        private Button btn;

        public ArticleOddViewHolder(@NonNull final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            mView = itemView;
        }

        public void onBind(final ArticleResponse article) {
            Glide.with(mView.getContext())
                    .load(article.getUrlToImage())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(imgUrl);
            tvAuthor.setText(article.getAuthor());
            tvTitle.setText(article.getTitle());
            tvPublishedAt.setText(article.getPublishedAt());

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventClick(article, v.getContext());
                }
            });
        }
    }

    public class ArticleEvenViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        @BindView(R.id.image_url1) ImageView img_url;
        @BindView(R.id.text_view_title1) TextView tv_title;
        @BindView(R.id.text_view_author1) TextView tv_author;
        @BindView(R.id.text_view_published_at1) TextView tv_published_at;
        private View mView;

        public ArticleEvenViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            mView = itemView;
            itemView.setOnCreateContextMenuListener(this);
        }

        public void onBind(final ArticleResponse article) {
            Glide.with(mView)
                    .load(article.getUrlToImage())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(img_url);
            tv_title.setText(article.getTitle());
            tv_author.setText(article.getAuthor());
            tv_published_at.setText(article.getPublishedAt());

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventClick(article, v.getContext());
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Call");
            menu.add(0, v.getId(), 0, "SMS");
        }


    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress_bar) ProgressBar progressBar;

        public ProgressViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public void eventClick(ArticleResponse article, Context context) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("article", article);

        Intent intent = new Intent(context, DetailArticleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("data", bundle);
        context.startActivity(intent);
    }
}
