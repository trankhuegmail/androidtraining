package com.tdk.baomoi_rgg.ui.article;

import android.content.Context;
import android.view.View;

import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

public interface ArticleFragmentMvpView {
    void init();
    void showError(String message);
    void loadData(ArticlesResponse articlesResponse);
}
