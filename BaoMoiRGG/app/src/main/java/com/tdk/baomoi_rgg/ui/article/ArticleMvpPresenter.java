package com.tdk.baomoi_rgg.ui.article;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;

public interface ArticleMvpPresenter {
    void start();
    void setDefaultTabLayout(TabLayout mTabLayout, int mIndex, FragmentManager mFragmentManager, Fragment mFragment);
    void loadArticle(FragmentManager mFragmentManager, Fragment mFragment, int id);
}
