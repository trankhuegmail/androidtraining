package com.tdk.baomoi_rgg.ui.history;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.tdk.baomoi_rgg.data.db.model.Article;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class HistoryPresenter implements HistoryMvpPresenter {
    private HistoryMvpView view;

    public HistoryPresenter(HistoryMvpView view) {
        this.view = view;
    }

    @Override
    public Bundle getBundle(Intent intent, String kewWord) {
        Bundle bundle = intent.getBundleExtra(kewWord);
        return bundle;
    }

    @Override
    public RealmResults<Article> getDataRealm(Realm realm) {
        RealmResults<Article> articles = realm.where(Article.class).findAll();
        return articles;
    }

    @Override
    public void deleteDataRealm(Realm realm, ArrayList<Article> articles, int position, String field) {
        realm.beginTransaction();

        Article result = realm.where(Article.class)
                .equalTo(field, articles.get(position).getId())
                .findFirst();
        result.deleteFromRealm();

        realm.commitTransaction();
    }

    @Override
    public void updateDataRealm(Realm realm, String...params) {

    }

    @Override
    public void removeItemAdapter(RecyclerView.Adapter adapter,ArrayList<Article> articles, int position) {
        articles.remove(position);
//      mRecyclerView.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, articles.size());
        adapter.notifyDataSetChanged();
    }
}
