package com.tdk.baomoi_rgg.ui.edit_history;

import android.content.Intent;
import android.os.Bundle;

public interface EditHistoryMvpPresenter {
    void start();
    Bundle getData(Intent intent, String key);
    void loadContent(Bundle bundle);
}
