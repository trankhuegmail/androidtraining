package com.tdk.baomoi_rgg.ui.details_article;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.db.model.Article;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class DetailArticleActivity extends AppCompatActivity implements DetailArticleMvpView {
    private ArticleResponse article;
    @BindView(R.id.image_url) ImageView mImageUrl;
    @BindView(R.id.text_view_author) TextView mAuthor;
    @BindView(R.id.text_view_published_at) TextView mPublishedAt;
    @BindView(R.id.text_view_title) TextView mTitle;
    @BindView(R.id.text_view_content) TextView mContent;
    private Unbinder unbinder;
    private Realm mRealm;
    private DetailArticlePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setTitle(R.string.activity_detail_article_title_bar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);

        unbinder = ButterKnife.bind(this);
        initPresenter();
        Intent data = getIntent();
        if(mPresenter.getBundle(data, "data") != null) {
            article = (ArticleResponse) mPresenter.getBundle(data, "data").getSerializable("article");
            setContent(article);
            initRealm(article);
        }
    }

    private void initPresenter() {
        mPresenter = new DetailArticlePresenter(this);
    }

    private void initRealm(ArticleResponse articleResponse) {
        Realm.init(this);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("history.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();

        mRealm = Realm.getInstance(configuration);

        RealmResults<Article> articles = mRealm.where(Article.class).findAll();
        if (articles.size() == 0) {
            mPresenter.createDataRealm(mRealm, articleResponse, 1, DetailArticleActivity.this);
        } else {
            int mNumberId = mRealm.where(Article.class).max("id").intValue();
            mPresenter.createDataRealm(mRealm, articleResponse, mNumberId+1, DetailArticleActivity.this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!mRealm.isClosed()) {
            mRealm.close();
        }
        unbinder.unbind();
    }

    @Override
    public void setContent(ArticleResponse article) {
        mContent.setText(article.getContent());
        mAuthor.setText(article.getAuthor());
        mTitle.setText(article.getTitle());
        mPublishedAt.setText(article.getPublishedAt());
        Glide.with(DetailArticleActivity.this)
                .load(article.getUrlToImage())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(mImageUrl);
    }
}
