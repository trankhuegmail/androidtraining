package com.tdk.baomoi_rgg.ui.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.db.model.Article;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private ArrayList<Article> articles;
    private Context context;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    private boolean deleted = false;

    public HistoryAdapter(ArrayList<Article> articles, Context context) {
        this.articles = articles;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new HistoryAdapter.HistoryViewHolde(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HistoryAdapter.HistoryViewHolde) {
            ((HistoryViewHolde) holder).onBind(articles.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return articles == null ? 0 : articles.size();
    }

    public class HistoryViewHolde extends RecyclerView.ViewHolder {
        @BindView(R.id.image_url)
        ImageView mImageUrl;
        @BindView(R.id.text_view_author)
        TextView mAuthor;
        @BindView(R.id.text_view_title)
        TextView mTitle;
        @BindView(R.id.text_view_published_at)
        TextView mPublishedAt;
        private View mView;
        @BindView(R.id.linear_layout_item_history)
        LinearLayout mLinearLayout;

        public HistoryViewHolde(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            mView = itemView;
        }

        public void onBind(final Article article) {
            mAuthor.setText(article.getAuthor());
            mTitle.setText(article.getTitle());
            mPublishedAt.setText(article.getPublishedAt());
            Glide.with(mView.getContext())
                    .load(article.getUrlToImage())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(mImageUrl);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                        onItemClickListener.onItemClick(itemView, getAdapterPosition());
                    }
//
//                    PopupMenu mPopupMenu = new PopupMenu(v.getContext(), mView);
//                    mPopupMenu.getMenuInflater().inflate(R.menu.menu_article_odd, mPopupMenu.getMenu());
//                    mPopupMenu.show();
//                    mPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//                            switch (item.getItemId()) {
//                                case R.id.edit:
//                                    Toast.makeText(v.getContext(), "edit", Toast.LENGTH_SHORT).show();
//                                    return true;
//                                case R.id.delete:
//                                    RealmConfiguration configuration = new RealmConfiguration.Builder()
//                                            .name("history.realm")
//                                            .schemaVersion(1)
//                                            .deleteRealmIfMigrationNeeded()
//                                            .build();
//
//                                    Realm mRealm = Realm.getInstance(configuration);
//
//                                    mRealm.beginTransaction();
//
//                                    Article result = mRealm.where(Article.class).equalTo("id", article.getId()).findFirst();
//                                    result.deleteFromRealm();
//
//                                    mRealm.commitTransaction();
//                                    setDeleted(true);
//
////                                    mRealm.executeTransaction(new Realm.Transaction() {
////                                        @Override
////                                        public void execute(Realm realm) {
////                                            Article result = realm.where(Article.class).equalTo("id", article.getId()).findFirst();
////                                            result.deleteFromRealm();
////                                        }
////                                    });
//
////                                    mRealm.executeTransactionAsync(new Realm.Transaction() {
////                                        @Override
////                                        public void execute(Realm realm) {
////                                            Article result = realm.where(Article.class).equalTo("id", article.getId()).findFirst();
////                                            result.deleteFromRealm();
////                                        }
////                                    }, new Realm.Transaction.OnSuccess() {
////                                        @Override
////                                        public void onSuccess() {
////                                            setDeleted(true);
////                                        }
////                                    });
//                                    return true;
//                                default:
//                                    return false;
//                            }
//                        }
//                    });
                }
            });
        }
    }
}
