package com.tdk.baomoi_rgg.ui.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.network.ApiNewsapi;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;
import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;
import com.tdk.baomoi_rgg.ui.adapter.ArticleAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArticleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_QUERY_COUNTRY = "country";
    private static final String ARG_QUERY_APIKEY  = "apiKey";

    // TODO: Rename and change types of parameters
    private String mCountry;
    private String mApiKey;

    public ArticleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param params List parameter.
     * @return A new instance of fragment ArticleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticleFragment newInstance(String...params) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_QUERY_COUNTRY, params[0]);
        args.putString(ARG_QUERY_APIKEY, "a65ca180cf2d461ebb9ea73c9cb680d0");
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCountry = getArguments().getString(ARG_QUERY_COUNTRY);
            mApiKey  = getArguments().getString(ARG_QUERY_APIKEY);
        }

        mRetrofit = new Retrofit.Builder()
                .baseUrl(PATH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mArticles = new ArrayList<>();
    }

    // Begin
    private final static String PATH_URL = "http://newsapi.org";
    private final static Integer PAGE_SIZE = 20;

    private static Integer mPageIndex = 1;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    private Retrofit mRetrofit;
    private ApiNewsapi mApiNewsapi;
    private ArrayList<ArticleResponse> mArticles;
    private LinearLayoutManager mLinearLayoutManager;
    private ArticleAdapter mAdapter;
    private boolean loading;
    private Unbinder mUnbinder;
    private int mArticleTotal;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_article, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        mApiNewsapi = mRetrofit.create(ApiNewsapi.class);

        mRecyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        Call<ArticlesResponse> call = mApiNewsapi.listArticle(mCountry, mApiKey, mPageIndex, PAGE_SIZE);
        call.enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        ArticlesResponse mArticlesResponse = response.body();
                        for (int i=0; i<mArticlesResponse.getArticleResponses().size(); i++) {
                            mArticles.add(i, mArticlesResponse.getArticleResponses().get(i));
                        }
                        mAdapter = new ArticleAdapter(mArticles, getContext());
                        mRecyclerView.setAdapter(mAdapter);

                        mArticleTotal = mArticlesResponse.getArticleTotal();

                        eventScroll();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {

            }
        });
//        loadArticle();
        return view;
    }

    private void loadArticle() {
        Call<ArticlesResponse> call = mApiNewsapi.getArticleList("movies", "079dac74a5f94ebdb990ecf61c8854b7"
                , mPageIndex, PAGE_SIZE);
        call.enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        ArticlesResponse mArticlesResponse = response.body();
                        for (int i=0; i<mArticlesResponse.getArticleResponses().size(); i++) {
                            mArticles.add(i, mArticlesResponse.getArticleResponses().get(i));
                        }
                        mAdapter = new ArticleAdapter(mArticles, getContext());
                        mRecyclerView.setAdapter(mAdapter);

                        eventScroll();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {

            }
        });
    }

    private void eventScroll() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (!loading) {
                        if (mLayoutManager != null
                                && mLayoutManager.findLastCompletelyVisibleItemPosition() == mLayoutManager.getItemCount() - 1) {
                            loadMore(mLayoutManager);
                        }
                    }
                }
            }
        });
    }

    private void loadMore(LinearLayoutManager mLayoutManager) {
        mPageIndex++;
        if (mPageIndex <= PAGE_SIZE && mLayoutManager.getItemCount() < mArticleTotal) {
            mArticles.add(null);
            mAdapter.notifyItemInserted(mArticles.size() - 1);

            Call<ArticlesResponse> call = mApiNewsapi.listArticle(mCountry, mApiKey, mPageIndex, 20);
            call.enqueue(new Callback<ArticlesResponse>() {
                @Override
                public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                    if (response.isSuccessful()) {
                        if (response != null) {
                            final ArticlesResponse mArticlesResponse = response.body();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mArticles.remove(mArticles.size() - 1);
                                    mAdapter.notifyItemRemoved(mArticles.size());

                                    final int start = mArticles.size();
                                    for (int i=0; i<mArticlesResponse.getArticleResponses().size(); i++) {
                                        mArticles.add(start+i, mArticlesResponse.getArticleResponses().get(i));
                                        mAdapter.notifyItemInserted(start+i);
                                    }

                                    loading = false;
                                }
                            }, 2000);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArticlesResponse> call, Throwable t) {

                }
            });

//            Call<ArticlesResponse> call2 = mApiNewsapi.getArticleList("movies", "079dac74a5f94ebdb990ecf61c8854b7", PAGE_INDEX, 20);
//            call2.enqueue(new Callback<ArticlesResponse>() {
//                @Override
//                public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
//                    if (response.isSuccessful()) {
//                        if (response != null) {
//                            final ArticlesResponse mArticlesResponse = response.body();
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    mArticles.remove(mArticles.size() - 1);
//                                    mAdapter.notifyItemRemoved(mArticles.size());
//
//                                    final int start = mArticles.size();
//                                    for (int i=0; i<mArticlesResponse.getArticleResponses().size(); i++) {
//                                        mArticles.add(start+i, mArticlesResponse.getArticleResponses().get(i));
//                                        mAdapter.notifyItemInserted(start+i);
//                                    }
//
//                                    loading = false;
//                                }
//                            }, 2000);
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ArticlesResponse> call, Throwable t) {
//
//                }
//            });

            loading = true;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        view = null;
    }
}
