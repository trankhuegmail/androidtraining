package com.tdk.baomoi_rgg.ui.edit_history;

import android.os.Bundle;

public interface EditHistoryMvpView {
    void init();
    void setContent(Bundle bundle);
}
