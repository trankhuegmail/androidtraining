package com.tdk.baomoi_rgg.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticlesResponse {
    @SerializedName("articles")
    private List<ArticleResponse> articleResponses;

    @SerializedName("totalResults")
    private int articleTotal;

    public int getArticleTotal() {
        return articleTotal;
    }

    public void setArticleTotal(int articleTotal) {
        this.articleTotal = articleTotal;
    }

    public List<ArticleResponse> getArticleResponses() {
        return articleResponses;
    }

    public void setArticleResponses(List<ArticleResponse> articleResponses) {
        this.articleResponses = articleResponses;
    }
}
