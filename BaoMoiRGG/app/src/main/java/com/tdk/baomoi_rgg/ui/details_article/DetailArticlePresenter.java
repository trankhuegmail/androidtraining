package com.tdk.baomoi_rgg.ui.details_article;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.tdk.baomoi_rgg.data.AppDataManager;
import com.tdk.baomoi_rgg.data.DataManager;
import com.tdk.baomoi_rgg.data.db.DbHelper;
import com.tdk.baomoi_rgg.data.db.model.Article;
import com.tdk.baomoi_rgg.data.network.ApiHelper;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

import io.realm.Realm;

public class DetailArticlePresenter implements DetailArticleMvpPresenter {
    private DetailArticleMvpView view;
//    private AppDataManager appDataManager;
//    private DbHelper dbHelper;
//    private ApiHelper apiHelper;

    public DetailArticlePresenter(DetailArticleMvpView view) {
        this.view = view;
    }

    @Override
    public Bundle getBundle(Intent intent, String kewWord) {
        Bundle bundle = intent.getBundleExtra(kewWord);
        return bundle;
    }

    @Override
    public void createDataRealm(Realm realm, ArticleResponse articleResponse, int id, Context context) {
//        appDataManager = new AppDataManager(context, dbHelper, apiHelper);
//        appDataManager.insertArticle(realm, articleResponse, id);
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Article article = realm.createObject(Article.class);
                article.setId(id);
                article.setAuthor(articleResponse.getAuthor());
                article.setTitle(articleResponse.getTitle());
                article.setUrlToImage(articleResponse.getUrlToImage());
                article.setPublishedAt(articleResponse.getPublishedAt());
                article.setContent(articleResponse.getContent());
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Toast.makeText(context, "Add success", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
