package com.tdk.baomoi_rgg.data;

import com.tdk.baomoi_rgg.data.db.DbHelper;
import com.tdk.baomoi_rgg.data.network.ApiHelper;

public interface DataManager extends DbHelper, ApiHelper {
}
