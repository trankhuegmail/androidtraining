package com.tdk.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiOfMine {
    @GET("/Views/Demo/retrofit/index.php")
    Call<Products> getProducts();

    @FormUrlEncoded
    @POST("/Views/Demo/retrofit/create.php")
    Call<Products.Product> createProduct(@Field("id") Integer id);

    @FormUrlEncoded
    @POST("/Views/Demo/retrofit/delete.php")
    Call<Products.Product> deleteProduct(@Field("id") Integer id);
}
