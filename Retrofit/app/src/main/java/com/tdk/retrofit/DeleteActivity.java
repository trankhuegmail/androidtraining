package com.tdk.retrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeleteActivity extends AppCompatActivity {
    EditText editTextIdProduct;
    Button buttonSave;
    Button buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);

        editTextIdProduct = findViewById(R.id.edit_text_id_product);
        buttonSave = findViewById(R.id.button_save);
        buttonBack = findViewById(R.id.button_back);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://wweshoptrankhue374.000webhostapp.com")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                ApiOfMine apiOfMine = retrofit.create(ApiOfMine.class);

                Call<Products.Product> call = apiOfMine.deleteProduct(Integer.parseInt(editTextIdProduct.getText().toString()));

                call.enqueue(new Callback<Products.Product>() {
                    @Override
                    public void onResponse(Call<Products.Product> call, Response<Products.Product> response) {
                        Log.d("TAG", "onResponse: " + response.code());
                    }

                    @Override
                    public void onFailure(Call<Products.Product> call, Throwable t) {
                        Toast.makeText(DeleteActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED);
    }
}
