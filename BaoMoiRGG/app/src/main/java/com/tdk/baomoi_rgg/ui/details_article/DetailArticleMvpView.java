package com.tdk.baomoi_rgg.ui.details_article;

import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

public interface DetailArticleMvpView {
    void setContent(ArticleResponse article);
}
