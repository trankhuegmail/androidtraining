package com.tdk.baomoi_rgg.ui.edit_history;

import android.content.Intent;
import android.os.Bundle;

public class EditHistoryPresenter implements EditHistoryMvpPresenter {
    private EditHistoryMvpView mView;

    public EditHistoryPresenter(EditHistoryMvpView mView) {
        this.mView = mView;
    }

    @Override
    public void start() {
        mView.init();
    }

    @Override
    public Bundle getData(Intent intent, String key) {
        return intent.getBundleExtra(key);
    }

    @Override
    public void loadContent(Bundle bundle) {
        mView.setContent(bundle);
    }
}
