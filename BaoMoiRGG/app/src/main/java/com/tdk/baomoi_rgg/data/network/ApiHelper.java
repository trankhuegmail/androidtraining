package com.tdk.baomoi_rgg.data.network;

import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiHelper {
    @GET("v2/top-headlines")
    Observable<ArticlesResponse> getArticlesResponse(
            @Query("country") String country,
            @Query("apiKey") String apiKey,
            @Query("page") int page,
            @Query("pageSize") int pageSize);
}
