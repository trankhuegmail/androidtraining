package com.tdk.baomoi_rgg.ui.edit_history;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.db.model.Article;
import com.tdk.baomoi_rgg.ui.details_article.DetailArticleActivity;
import com.tdk.baomoi_rgg.ui.history.HistoryActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class EditHistoryActivity extends AppCompatActivity implements EditHistoryMvpView {
//    @BindView(R.id.edit_text_title)
//    EditText mEditTextTitle;
//    @BindView(R.id.button_save)
//    Button mButtonSave;
//    @BindView(R.id.button_cancel)
//    Button mButtonCancel;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;

    @BindView(R.id.image_url) ImageView mImageUrl;
    @BindView(R.id.text_view_author) TextView mAuthor;
    @BindView(R.id.text_view_published_at) TextView mPublishedAt;
    @BindView(R.id.text_view_title) TextView mTitle;
    @BindView(R.id.text_view_content) TextView mContent;
    @BindView(R.id.edit_text_title) EditText mEditTitle;
    @BindView(R.id.button_edit) Button mButtonEdit;
    private Unbinder unbinder;
    private int mId;
    private Realm mRealm;
    private EditHistoryPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setTitle(R.string.activity_edit_history_title_bar);
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_history);
        setContentView(R.layout.activity_detail_article);

        unbinder = ButterKnife.bind(this);
//        setActionBar(toolbar);

//        Realm.init(this);
//        RealmConfiguration configuration = new RealmConfiguration.Builder()
//                .name("history.realm")
//                .schemaVersion(1)
//                .deleteRealmIfMigrationNeeded()
//                .build();
//        mRealm = Realm.getInstance(configuration);

        initPresenter();

        mButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTitle.isShown()) {
                    mTitle.setVisibility(View.GONE);
                    mEditTitle.setVisibility(View.VISIBLE);
                    mEditTitle.setText(mTitle.getText());
                }else if (mEditTitle.isShown()) {
                    mEditTitle.setVisibility(View.GONE);
                    mTitle.setVisibility(View.VISIBLE);
                    mTitle.setText(mEditTitle.getText());
                    Article article = mRealm.where(Article.class).equalTo("id", mId).findFirst();
                    mRealm.beginTransaction();
                    article.setTitle(String.valueOf(mEditTitle.getText()));
                    mRealm.commitTransaction();
                }
            }
        });
    }

    private void initPresenter() {
        Intent data = getIntent();
        mPresenter = new EditHistoryPresenter(this);
        mPresenter.start();
        mPresenter.loadContent(mPresenter.getData(data, "data"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void init() {
        unbinder = ButterKnife.bind(this);
//        setActionBar(toolbar);

        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("history.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        mRealm = Realm.getInstance(configuration);
    }

    @Override
    public void setContent(Bundle bundle) {
        try {
            String mStrTitle       = bundle.getString("title");
            String mStrAuthor      = bundle.getString("author");
            String mStrPublishedAt = bundle.getString("published_at");
            String mStrContent     = bundle.getString("content");
            String mStrUrlToImage  = bundle.getString("url_to_image");
            mId                    = bundle.getInt("id");

            mContent.setText(mStrContent);
            mAuthor.setText(mStrAuthor);
            mTitle.setText(mStrTitle);
            mPublishedAt.setText(mStrPublishedAt);
            Glide.with(EditHistoryActivity.this)
                    .load(mStrUrlToImage)
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(mImageUrl);
        } catch (Exception e) {

        }
    }
}
