package com.tdk.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class Products {
    @SerializedName("total")
    private Integer total;

    @SerializedName("products")
    List<Product> products;

    public Integer getTotal() {
        return total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public final class Product {
        @SerializedName("idProduct")
        private Integer idProduct;

        public Integer getIdProduct() {
            return idProduct;
        }
    }
}
