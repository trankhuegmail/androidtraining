package com.tdk.baomoi_rgg.ui.details_article;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

import io.realm.Realm;

public interface DetailArticleMvpPresenter {
    Bundle getBundle(Intent intent, String kewWord);
    void createDataRealm(Realm realm, ArticleResponse articleResponse, int id, Context context);
}
