package com.tdk.baomoi_rgg.ui.article;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.ui.history.HistoryActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ArticleActivity extends AppCompatActivity implements ArticleMvpView {
    @BindView(R.id.tab_layout) TabLayout mTabLayout;
    private final int mIndexTabLayout = 2;
    private Fragment mFragment;
    private Fragment mFragmentTh;
    private Fragment mFragmentUk;
    private Fragment mFragmentUs;
    private Fragment mFragmentSi;
    private Fragment mFragmentFr;
    private Unbinder unbinder;

    public static boolean exit = false;

    private ArticlePresenter mArticlePresenter;

    private static final String TAG = ArticleActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        initPresenter();
        Intent intent = new Intent();
        intent.setAction("test.Broadcast");
        sendBroadcast(intent);
    }

    private void initPresenter() {
        mArticlePresenter = new ArticlePresenter(this);
        mArticlePresenter.start();
        mArticlePresenter.setDefaultTabLayout(mTabLayout, mIndexTabLayout, getSupportFragmentManager(), mFragment);

        tabSelected();
    }

    private void tabSelected() {
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        mFragment = mFragmentTh;
                        break;
                    case 1:
                        mFragment = mFragmentUk;
                        break;
                    case 2:
                        mFragment = mFragmentUs;
                        break;
                    case 3:
                        mFragment = mFragmentSi;
                        break;
                    case 4:
                        mFragment = mFragmentFr;
                        break;
                }

                mArticlePresenter.loadArticle(getSupportFragmentManager(), mFragment, R.id.frame_layout);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void init() {
        unbinder = ButterKnife.bind(this);

        mFragmentUs = new ArticleFragment().newInstance("us");
        mFragmentUk = new ArticleFragment().newInstance("gb");
        mFragmentTh = new ArticleFragment().newInstance("th");
        mFragmentSi = new ArticleFragment().newInstance("si");
        mFragmentFr = new ArticleFragment().newInstance("fr");
    }

    @Override
    public void showTabLayout(TabLayout mTabLayout, int mIndex, FragmentManager mFragmentManager, Fragment mFragment) {
        mTabLayout.addTab(mTabLayout.newTab().setText("TH"));
        mTabLayout.addTab(mTabLayout.newTab().setText("UK"));
        mTabLayout.addTab(mTabLayout.newTab().setText("US"));
        mTabLayout.addTab(mTabLayout.newTab().setText("SI"));
        mTabLayout.addTab(mTabLayout.newTab().setText("FR"));

        mTabLayout.getTabAt(mIndex).select();
        if (mTabLayout.getTabAt(mIndex).isSelected()) {
            mFragment = new ArticleFragment().newInstance("us");
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.frame_layout, mFragment).commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_item_history:
                intent = new Intent(ArticleActivity.this, HistoryActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exit = true;
    }
}
