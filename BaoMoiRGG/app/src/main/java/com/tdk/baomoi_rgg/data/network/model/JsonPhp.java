package com.tdk.baomoi_rgg.data.network.model;

import com.google.gson.annotations.SerializedName;

public class JsonPhp {
    @SerializedName("idProduct")
    private String idProduct;

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }
}
