package com.tdk.baomoi_rgg.data.db;

import com.tdk.baomoi_rgg.data.db.model.Article;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

import io.realm.Realm;

public class AppDbHelper implements DbHelper {

    @Override
    public void insertArticle(Realm realm, ArticleResponse article, int id) {
        realm.beginTransaction();

        Article article1 = realm.createObject(Article.class);
        article1.setId(id);
        article1.setAuthor(article.getAuthor());
        article1.setTitle(article.getTitle());
        article1.setUrlToImage(article.getUrlToImage());
        article1.setPublishedAt(article.getPublishedAt());
        article1.setContent(article.getContent());

        realm.commitTransaction();
    }

    @Override
    public void deleteArticle(Realm realm, int id) {
        realm.beginTransaction();

        Article article = realm.where(Article.class)
                .equalTo("id", id)
                .findFirst();

        article.deleteFromRealm();

        realm.commitTransaction();
    }

    @Override
    public void updateArticle(Realm realm, String field, int id) {
    }
}
