package com.tdk.baomoi_rgg.ui.article;

import android.util.Log;

import com.tdk.baomoi_rgg.data.network.ApiEndpoint;
import com.tdk.baomoi_rgg.demo.ApiNewsapi;
import com.tdk.baomoi_rgg.data.network.RetrofitAdapter;
import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ArticleFragmentPresenter implements ArticleFragmentMvpPresenter {
    private ArticleFragmentMvpView mView;

    public ArticleFragmentPresenter(ArticleFragmentMvpView mView) {
        this.mView = mView;
    }

    @Override
    public void start() {
        mView.init();
    }

    @Override
    public void getArticles(CompositeDisposable compositeDisposable, String country, String apiKey, int pageIndex, int pageSize) {
        ApiNewsapi apiNewsapi = RetrofitAdapter.getInstance(ApiEndpoint.NEWS_API).create(ApiNewsapi.class);

        compositeDisposable.add(apiNewsapi.getArticlesResponse(country, apiKey, pageIndex, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArticlesResponse>() {
                    @Override
                    public void onNext(ArticlesResponse articlesResponse) {
                        Log.d("TAG", "onNext: "+articlesResponse.getArticleResponses().get(0).getTitle());
                        mView.loadData(articlesResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("TAG", "onError: " + e.getMessage());
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("TAG", "onComplete: Complete");
                    }
                }));
    }
}
