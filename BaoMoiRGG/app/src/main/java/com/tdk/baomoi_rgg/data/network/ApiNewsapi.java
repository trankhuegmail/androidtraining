package com.tdk.baomoi_rgg.data.network;

import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiNewsapi {
    @GET("v2/everything")
    Call<ArticlesResponse> getArticleList(@Query("q") String q
            , @Query("apiKey") String apiKey, @Query("page") int page
            , @Query("pageSize") int pageSize);

    @GET("v2/top-headlines")
    Call<ArticlesResponse> listArticle(@Query("country") String country
            , @Query("apiKey") String apiKey, @Query("page") int page
            , @Query("pageSize") int pageSize);
}
