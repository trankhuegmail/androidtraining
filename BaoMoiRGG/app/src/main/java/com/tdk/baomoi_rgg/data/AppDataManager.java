package com.tdk.baomoi_rgg.data;

import android.content.Context;

import com.tdk.baomoi_rgg.data.db.DbHelper;
import com.tdk.baomoi_rgg.data.network.ApiHelper;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;
import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

import io.reactivex.Observable;
import io.realm.Realm;

public class AppDataManager implements DataManager {
    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mHelper;
    private final ApiHelper mApiHelper;

    public AppDataManager(Context mContext, DbHelper mHelper, ApiHelper mApiHelper) {
        this.mContext = mContext;
        this.mHelper = mHelper;
        this.mApiHelper = mApiHelper;
    }

    @Override
    public Observable<ArticlesResponse> getArticlesResponse(String country, String apiKey, int page, int pageSize) {
        return mApiHelper.getArticlesResponse(country, apiKey, page, pageSize);
    }

    @Override
    public void insertArticle(Realm realm, ArticleResponse article, int id) {
        mHelper.insertArticle(realm, article, id);
    }

    @Override
    public void deleteArticle(Realm realm, int id) {
        mHelper.deleteArticle(realm, id);
    }

    @Override
    public void updateArticle(Realm realm, String field, int id) {
        mHelper.updateArticle(realm, field, id);
    }
}
