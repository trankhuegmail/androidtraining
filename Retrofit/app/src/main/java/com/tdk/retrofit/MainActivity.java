package com.tdk.retrofit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    Retrofit retrofit;
    TextView responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://wweshoptrankhue374.000webhostapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiOfMine apiOfMine = retrofit.create(ApiOfMine.class);

        Call<Products> call = apiOfMine.getProducts();

        call.enqueue(new Callback<Products>() {
            @Override
            public void onResponse(Call<Products> call, Response<Products> response) {
                Products products = response.body();

                for (int i = 0; i < products.getProducts().size(); i++) {
                    Log.d("ID-PRODUCT", "onResponse: " + products.getProducts().get(i).getIdProduct());
                }

                Toast.makeText(MainActivity.this, products.getTotal().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Products> call, Throwable t) {
                Toast.makeText(MainActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_header, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_create:
                intent = new Intent(MainActivity.this, CreateActivity.class);
                startActivityForResult(intent, 123);
                break;
            case R.id.menu_delete:
                intent = new Intent(MainActivity.this, DeleteActivity.class);
                startActivityForResult(intent, 456);
        }
        return super.onOptionsItemSelected(item);
    }
}
