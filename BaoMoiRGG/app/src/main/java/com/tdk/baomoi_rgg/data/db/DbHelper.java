package com.tdk.baomoi_rgg.data.db;

import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;

import io.realm.Realm;

public interface DbHelper {
    void insertArticle(Realm realm, ArticleResponse article, int id);
    void deleteArticle(Realm realm, int id);
    void updateArticle(Realm realm, String field, int id);
}
