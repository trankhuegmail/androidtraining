package com.tdk.baomoi_rgg.ui.article;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tdk.baomoi_rgg.R;
import com.tdk.baomoi_rgg.data.network.ApiEndpoint;
import com.tdk.baomoi_rgg.demo.ApiNewsapi;
import com.tdk.baomoi_rgg.data.network.RetrofitAdapter;
import com.tdk.baomoi_rgg.data.network.model.ArticleResponse;
import com.tdk.baomoi_rgg.data.network.model.ArticlesResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArticleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFragment extends Fragment implements ArticleFragmentMvpView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_QUERY_COUNTRY = "country";
    private static final String ARG_QUERY_APIKEY  = "apiKey";

    // TODO: Rename and change types of parameters
    private String mCountry;
    private String mApiKey;

    public ArticleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param params List parameter.
     * @return A new instance of fragment ArticleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticleFragment newInstance(String...params) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_QUERY_COUNTRY, params[0]);
        args.putString(ARG_QUERY_APIKEY, "a65ca180cf2d461ebb9ea73c9cb680d0");
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCountry = getArguments().getString(ARG_QUERY_COUNTRY);
            mApiKey  = getArguments().getString(ARG_QUERY_APIKEY);
        }
    }

    // Begin
    private final static Integer PAGE_SIZE = 20;

    private static Integer mPageIndex = 1;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    private Retrofit mRetrofit;
    private ApiNewsapi mApiNewsapi;
    private ArrayList<ArticleResponse> mArticles;
    private LinearLayoutManager mLinearLayoutManager;
    private ArticleAdapter mAdapter;
    private boolean loading;
    private Unbinder unbinder;
    private int mArticleTotal;
    private View view;

    private ArticleFragmentPresenter mPresenter;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_article, container, false);

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://wweshoptrankhue374.000webhostapp.com")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        ApiJsonPhp apiJsonPhp = retrofit.create(ApiJsonPhp.class);
//        Call<JsonPhp> call = apiJsonPhp.getId();
//
//        call.enqueue(new Callback<JsonPhp>() {
//            @Override
//            public void onResponse(Call<JsonPhp> call, Response<JsonPhp> response) {
//                JsonPhp jsonPhp = response.body();
//                Toast.makeText(getContext(), ""+jsonPhp.getIdProduct(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<JsonPhp> call, Throwable t) {
//                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

        mPresenter = new ArticleFragmentPresenter(this);
        mPresenter.start();

        // Retrofit
        loadArticle();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        compositeDisposable.clear();
    }

    private void loadArticle() {
        Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://newsapi.org")
        .addConverterFactory(GsonConverterFactory.create())
        .build();

        mApiNewsapi = retrofit.create(ApiNewsapi.class);

        mArticles = new ArrayList<>();

//        ArticlesResponse res = mApiNewsapi.listArticle(mCountry, mApiKey, mPageIndex, PAGE_SIZE).execute().body();

        Call<ArticlesResponse> call = mApiNewsapi.listArticle(mCountry, mApiKey, mPageIndex, PAGE_SIZE);
        call.enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                if (response.isSuccessful()) {
                    if (response != null) {
                        ArticlesResponse mArticlesResponse = response.body();
                        for (int i=0; i<mArticlesResponse.getArticleResponses().size(); i++) {
                            mArticles.add(i, mArticlesResponse.getArticleResponses().get(i));
                        }
                        mAdapter = new ArticleAdapter(mArticles, getContext());
                        mRecyclerView.setAdapter(mAdapter);

                        mArticleTotal = mArticlesResponse.getArticleTotal();

                        eventScroll();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {

            }
        });
    }

    private void eventScroll() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (!loading) {
                        if (mLayoutManager != null
                                && mLayoutManager.findLastCompletelyVisibleItemPosition() == mLayoutManager.getItemCount() - 1) {
                            loadMore(mLayoutManager);
                        }
                    }
                }
            }
        });
    }

    private void loadMore(LinearLayoutManager mLayoutManager) {
        mPageIndex++;
        if (mPageIndex <= PAGE_SIZE && mLayoutManager.getItemCount() < mArticleTotal) {
            mArticles.add(null);
            mAdapter.notifyItemInserted(mArticles.size() - 1);

            Call<ArticlesResponse> call = mApiNewsapi.listArticle(mCountry, mApiKey, mPageIndex, 20);
            call.enqueue(new Callback<ArticlesResponse>() {
                @Override
                public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                    if (response.isSuccessful()) {
                        if (response != null) {
                            final ArticlesResponse mArticlesResponse = response.body();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mArticles.remove(mArticles.size() - 1);
                                    mAdapter.notifyItemRemoved(mArticles.size());

                                    final int start = mArticles.size();
                                    for (int i=0; i<mArticlesResponse.getArticleResponses().size(); i++) {
                                        mArticles.add(start+i, mArticlesResponse.getArticleResponses().get(i));
                                        mAdapter.notifyItemInserted(start+i);
                                    }

                                    loading = false;
                                }
                            }, 2000);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArticlesResponse> call, Throwable t) {

                }
            });

            loading = true;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        view = null;
        compositeDisposable.clear();
    }

    @Override
    public void init() {
        unbinder = ButterKnife.bind(this, view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mPresenter.getArticles(compositeDisposable, mCountry, mApiKey, mPageIndex, PAGE_SIZE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadData(ArticlesResponse articlesResponse) {
        ArrayList<ArticleResponse> articles = new ArrayList<>();
        for (int i=0; i<articlesResponse.getArticleResponses().size(); i++) {
            articles.add(i, articlesResponse.getArticleResponses().get(i));
        }

        ArticleAdapter adapter = new ArticleAdapter(articles, getContext());
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    if (!loading) {
                        if (mLayoutManager != null
                                && mLayoutManager.findLastCompletelyVisibleItemPosition() == mLayoutManager.getItemCount() - 1) {

                            mPageIndex++;
                            mArticleTotal = articlesResponse.getArticleTotal();
                            if (mPageIndex <= PAGE_SIZE && mLayoutManager.getItemCount() < mArticleTotal) {
                                articles.add(null);
                                adapter.notifyItemInserted(articles.size() - 1);

                                ApiNewsapi apiNewsapi = RetrofitAdapter.getInstance(ApiEndpoint.NEWS_API).create(ApiNewsapi.class);

                                compositeDisposable.add(apiNewsapi.getArticlesResponse(mCountry, mApiKey, mPageIndex, PAGE_SIZE)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeWith(new DisposableObserver<ArticlesResponse>() {
                                            @Override
                                            public void onNext(ArticlesResponse articlesResponse) {
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        articles.remove(articles.size() - 1);
                                                        adapter.notifyItemRemoved(articles.size());

                                                        final int start = articles.size();
                                                        for (int i=0; i<articlesResponse.getArticleResponses().size(); i++) {
                                                            articles.add(start+i, articlesResponse.getArticleResponses().get(i));
                                                            adapter.notifyItemInserted(start+i);
                                                        }

                                                        loading = false;
                                                    }
                                                }, 2000);
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {

                                            }
                                        })
//                                        .subscribe(new Observer<ArticlesResponse>() {
//                                            @Override
//                                            public void onSubscribe(Disposable d) {
//
//                                            }
//
//                                            @Override
//                                            public void onNext(ArticlesResponse articlesResponse) {
//                                                Log.d("TAG", "onNext: "+articlesResponse.getArticleResponses().get(0).getTitle());
//                                                new Handler().postDelayed(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        articles.remove(articles.size() - 1);
//                                                        adapter.notifyItemRemoved(articles.size());
//
//                                                        final int start = articles.size();
//                                                        for (int i=0; i<articlesResponse.getArticleResponses().size(); i++) {
//                                                            articles.add(start+i, articlesResponse.getArticleResponses().get(i));
//                                                            adapter.notifyItemInserted(start+i);
//                                                        }
//
//                                                        loading = false;
//                                                    }
//                                                }, 2000);
//
//                                            }
//
//                                            @Override
//                                            public void onError(Throwable e) {
//                                                Log.d("TAG", "onError: " + e.getMessage());
//                                            }
//
//                                            @Override
//                                            public void onComplete() {
//                                                Log.d("TAG", "onComplete: Complete");
//                                            }
//                                        }
                            );

                                loading = true;
                            }
                        }
                    }
                }
            }
        });
    }
}
