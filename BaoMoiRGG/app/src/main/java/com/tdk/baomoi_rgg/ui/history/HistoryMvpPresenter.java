package com.tdk.baomoi_rgg.ui.history;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.tdk.baomoi_rgg.data.db.model.Article;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public interface HistoryMvpPresenter {
    Bundle getBundle(Intent intent, String kewWord);
    RealmResults<Article> getDataRealm(Realm realm);
    void deleteDataRealm(Realm realm, ArrayList<Article> articles, int position, String field);
    void updateDataRealm(Realm realm, String...params);
    void removeItemAdapter(RecyclerView.Adapter adapter, ArrayList<Article> articles, int position);
}
